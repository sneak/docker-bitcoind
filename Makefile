default: build

build: kill
	docker build -t sneak/bitcoind:latest .

run: kill
	docker run --name sneak-bitcoind -d sneak/bitcoind:latest
	docker logs -f sneak-bitcoind

kill:
	-docker rm -f sneak-bitcoind
