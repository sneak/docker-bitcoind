# sneak/bitcoind docker image

```
    __    _ __             _           __
   / /_  (_) /__________  (_)___  ____/ /
  / __ \/ / __/ ___/ __ \/ / __ \/ __  /
 / /_/ / / /_/ /__/ /_/ / / / / / /_/ /
/_.___/_/\__/\___/\____/_/_/ /_/\__,_/
```

* supports zmq
* wallet support disabled

# Notes

This outputs the config file, including the
rpc user password (either from `BITCOIND_RPCPASSWORD` or randomly
generated) to the logs on startup.

# Security

This container logs the rpc password into the container's logfiles.

# Versions

* sneak/bitcoind:latest -> bitcoind master
* sneak/bitcoind:v0.18.1 -> bitcoind v0.18.1

# Usage

```
docker run \
        --name bitcoind \
        -v /storage/bitcoin/bitcoind:/var/lib/bitcoind \
        --env BITCOIND_RPCUSER=username \
        --env BITCOIND_RPCPASSWORD=supersecret \
        -p 8333:8333 \
        -p 8332:8332 \
        -d sneak/bitcoind:v0.18.1

docker logs -f bitcoind
```
